AUTHORS = ["Fabio Durán-Verdugo <fabioduran@gnome.org>",
           "Alejandro Valdés-Jiménez <avaldes@gnome.org>",
           "Matías Rojas-Tapia <hackmati@hotmail.com>"]
ARTISTS = ["Daniel Galleguillos <daniel@gnome.cl>"]
WEBSITE = "https://gitlab.gnome.org/fabioduran/handibox/"
LOGO = "/org/gnome/handibox/images/handibox_426_x_240.png"

A11Y_CONFIGURATION_KEY = "org.gnome.handibox"
A11Y_APPLICATIONS_KEY = "org.gnome.desktop.a11y.applications"
A11Y_MOUSE_KEY = "org.gnome.desktop.a11y.mouse"

# GUI
UI_FILE = ""
