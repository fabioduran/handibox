#!/usr/bin/env python3

import random
import cairo
from gi.repository import Gio, Gtk
# from gi.repository.Gtk import Window
from .keypath import A11Y_CONFIGURATION_KEY, A11Y_MOUSE_KEY
from .device import Devices


class HandiboxExercise(Gtk.Dialog):
    def __init__(self, parent):
        Gtk.Dialog.__init__(self,
                            "Exercises",
                            parent,
                            0)

        # window parent.
        self.parent = parent
        self.resize(800, 600)
        # self.add(button)

        builder = Gtk.Builder()
        box = self.get_content_area()
        builder.add_from_resource("/org/gnome/handibox/handibox.ui")

        close_button = self.add_button(Gtk.STOCK_CLOSE,
                                       Gtk.ResponseType.CLOSE)
        close_button.set_always_show_image(True)

        box_exercise = builder.get_object("box_exercise")
        box.add(box_exercise)

        self.button = builder.get_object("btn_exercise")
        self.button.connect("clicked",
                            self.on_click_button)

        self.tran_setup()
        self.show_all()

    def on_click_button(self, btn=None):
        # FIXME
        top = random.randint(0, 600)
        left = random.randint(0, 600)
        self.button.set_margin_top(top)
        self.button.set_margin_left(left)

    def tran_setup(self):
        self.set_app_paintable(True)
        screen = self.get_screen()

        visual = screen.get_rgba_visual()
        if not visual and screen.is_composited():
            self.set_visual(visual)

    def on_draw(self, wid, cr):
        cr.set_source_rgba(0.2, 0.2, 0.2, 0.4)
        cr.set_operator(cairo.OPERATOR_SOURCE)
        cr.paint()

    def close_dialog(self, button):
        self.destroy()
