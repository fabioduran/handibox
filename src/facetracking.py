#!/usr/bin/env python3

import gi
gi.require_version('Gdk', '3.0')
from gi.repository import Gdk, Gio
from .device import Devices
import cv2 as cv
import os
import numpy as np
import pyautogui

import dlib

class FaceTracking:
    def __init__(self):

        self.config_key = Gio.Settings.new("org.gnome.handibox")
        self.config_key_a11y_mouse = Gio.Settings.new("org.gnome.desktop.a11y.mouse")

        ###DATA_FILE = ''.join([os.path.dirname(os.path.abspath(__file__)), "/haarcascade_frontalface_default.xml"])
        DATA_FILE = ''.join([os.path.dirname(os.path.abspath(__file__)), "/shape_predictor_68_face_landmarks.dat"])

        # radius for neutral circle (red color).
        self.radius_circle = 20
        # radius for control circle (green color).
        self.radius_control = 5

        #
        self.devices = Devices()

        self.calibration_threshold = self.config_key_a11y_mouse.get_int("dwell-threshold")

        # Haar classifier cascade
        ###self.faceCascade = cv.CascadeClassifier(DATA_FILE)
        self.predictor = dlib.shape_predictor(DATA_FILE)
        self.detector = dlib.get_frontal_face_detector()

        # Pointer coordinate
        self.xx = 0

        # Pointer coordinate
        self.yy = 0

        # Coordinates for draw face frame
        self.pt1 = 0

        # Coordinates for draw face frame
        self.pt2 = 0

        self.realscreenwidth, self.realscreenheight = self.devices.get_geometry()

        # Current coordinares of pointer on screen
        self.Data = {"current": ((int(self.realscreenwidth)/2),
                                 (int(self.realscreenheight)/2))}
        # Update pointer coordinates
        self.lastData = self.Data
        # Old coordinates
        self.olddata = {"old": (0, 0)}
        # Update pointer coordinates
        self.lastold = self.olddata

        #
        self.virtual_screen_x = self.config_key.get_int("widthvalue")/2  # Size of virtualscreen X axis
        self.virtual_screen_y = self.config_key.get_int("heightvalue")/2  # Size of virtualscreen y axis

    def detectFace(self, image):
        image = cv.flip(image, 1)
        
        # convert to gray.
        gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
       
        # Detect the faces.
        faces = self.detector(gray)

        # new transparent background image (replace original image).
        image = np.zeros((self.config_key.get_int("heightvalue"), self.config_key.get_int("widthvalue"), 4), dtype=np.uint8)

        # draw neutral circle (red color)
        center = (int(self.config_key.get_int("widthvalue")/2), int(self.config_key.get_int("heightvalue")/2))
        cv.circle(image, center, self.radius_circle, (255, 0, 0, 255), 2, 15, 0)

        if (len(faces) > 0):
            #
            landmarks = self.predictor(image=gray, box=faces[0])
            
            # https://www.studytonight.com/post/dlib-68-points-face-landmark-detection-with-opencv-and-python
            # 28 or 34 (point of the nose)
            self.xx = landmarks.part(28).x
            self.yy = landmarks.part(28).y

            # Draw central point in face (green color).
            color = (0, 255, 0, 255)
            cv.circle(image, (self.xx, self.yy), self.radius_control, color, -1, 15, 0)

            # add line (joystick)
            image = cv.line(image, center, (self.xx, self.yy), color, 5)

            # Write current pointer position
            self.Data["current"] = (self.xx, self.yy)
            
        return (image)

        """
        # Haarcascade param
        # Minimum possible object size. Objects smaller than that are ignored.
        min_size = (30, 30)

        # Parameter specifying how much the image size is reduced at each
        # image scale.
        image_scale = 2

        # The factor by which the search window is scaled between the
        # subsequent scans, 1.2 means increasing window by 12 %
        haar_scale = 1.2

        # Parameter specifying how many neighbors each candidate rectangle
        # should have to retain it
        min_neighbors = 2

        # If it is set, the function uses Canny edge detector to reject some
        # image regions that contain too few or too much edges and thus candidate
        # not contain the searched object.
        # haar_flags = 0

        # cv.Flip (image, image, 1) #Flip image
        image = cv.flip(image, 1)

        # Allocate the temporary images
        gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)

        # Scale input image for faster processing
        h, w, d = image.shape
        smallImage = cv.resize(gray,
                               (round(w/image_scale), round(h/image_scale)),
                               cv.INTER_LINEAR)

        # Equalize the histogram
        cv.equalizeHist(smallImage, smallImage)

        # Detect the faces
        face = self.faceCascade.detectMultiScale(smallImage,
                                                 scaleFactor=haar_scale,
                                                 minNeighbors=min_neighbors,
                                                 minSize=min_size)

        # new background image (replace original image).
        image = np.zeros((self.config_key.get_int("heightvalue"), self.config_key.get_int("widthvalue"), 4), dtype=np.uint8)
        # Draw neutral circle (red color)
        cv.circle(image, (int(self.config_key.get_int("widthvalue")/2), 
                          int(self.config_key.get_int("heightvalue")/2)), 
                          self.radius_circle, (255, 0, 0, 255), 2, 15, 0)

        # If face are found
        if len(face) > 0:
            # ((x, y, w, h), n) = face[0]
            (x, y, w, h) = face[0]

            # The input to cv.HaarDetectObjects was resized, so scale the
            # Bounding box of each face and convert it to two CvPoints
            self.pt1 = (x * image_scale, y * image_scale)
            self.pt2 = ((x + w) * image_scale, (y + h) * image_scale)

            # self.xx = (self.pt1[0] + self.pt2[0])/2
            self.xx = int((self.pt1[0] + self.pt2[0])/2)

            # self.yy = (self.pt1[1] + self.pt2[1])/2
            self.yy = int((self.pt1[1] + self.pt2[1])/2)

            # Draw central point in face (green color)
            ###cv.circle(image, (self.xx, self.yy), self.radius_control, (0, 255, 0, 0), -1, 15, 0)
            cv.circle(image, (self.xx, self.yy), self.radius_control, (0, 255, 0, 255), -1, 15, 0)

            # Draw neutral circle (red color)
            ###cv.circle(image, (int(self.config_key.get_int("widthvalue")/2), int(self.config_key.get_int("heightvalue")/2)), self.radius_circle, (255, 0, 0, 0), 2, 15, 0)

            # Write current pointer position
            self.Data["current"] = (self.xx, self.yy)

        return (image)
        """

    def updateMousePos(self):
        pos = self.Data["current"]
        self.moveMouse(pos[0], pos[1])

    def moveMouse(self, x, y):
        """ update and move pointer """
        # Get coordinates x & y of virtual screen from thread
        # on UpdateMousePos method

        oldx, oldy = self.olddata["old"]

        # 0,0 neutral zone... no movement
        if (x == oldx and y == oldy):
            return
        if (x <= 0 and y <= 0):
            return

        # Get current pointer coordinates
        px, py = self.devices.get_pointer_xy()
        px = int(px)
        py = int(py)

        diff_x = abs((self.virtual_screen_x) - x)
        diff_y = abs((self.virtual_screen_y) - y)

        if (diff_x <= self.radius_circle and diff_y <= self.radius_circle):
            return

        if (self.virtual_screen_x > x):
            px = (px) - self.calibration_threshold
        else:
            px = (px) + self.calibration_threshold

        if (self.virtual_screen_y > y):
            py = (py) - self.calibration_threshold
        else:
            py = (py) + self.calibration_threshold

        self.olddata["old"] = (x, y)

        # FIXME
        # pscreen = self.devices.get_screen()
        # device_pointer = self.devices.get_pointer()
        # device_pointer.warp(pscreen, px, py)
        pyautogui.moveTo(px, py)
