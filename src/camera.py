#!/usr/bin/env python3
#:PEP8 -E402

import cv2 as cv
import gi
import time
gi.require_version('GdkPixbuf', '2.0')
from gi.repository import GdkPixbuf
from gi.repository import GLib
from gi.repository import Gio
from .facetracking import FaceTracking

_GDK_PIXBUF_BIT_PER_SAMPLE = 8

class Camera:
    def __init__(self, camera_image):
        # GtkImage.
        self.camera_image = camera_image

        #
        self.config_key = Gio.Settings.new("org.gnome.handibox")

        # get camera id from settings.
        self.id_device = self.config_key.get_int("cameraid")

        # create capture object from selected camera.
        self.capture = cv.VideoCapture(self.id_device)

        # object to detect the face.
        self.faceTraking = FaceTracking()

        # for controlling the loop that captures images. Enter the loop.
        self.run = True

    def _init_thread(self):
        while self.run:
            GLib.idle_add(self.capture_loop)
            # FIXME: find another approach.
            time.sleep(0.1)

    def capture_loop(self):
        # capture image
        ret, image = self.capture.read()

        # captured an image?
        if (ret is True):
            # resizes the image using the size of the main window (Gtk.ApplicationWindow).
            image = cv.resize(image, (self.config_key.get_int("widthvalue"), 
                                      self.config_key.get_int("heightvalue")))

            # detect face. the image could include a green point and a blue circle in the center.
            frame = self.faceTraking.detectFace(image)

            # updates mouse position
            self.faceTraking.updateMousePos()

            # prepares the image to add it into the GtkImage
            hh, ww, channels = frame.shape
            if channels == 3:
                cvimage = cv.cvtColor(frame, cv.COLOR_RGB2BGR)
            else:
                cvimage = cv.cvtColor(frame, cv.COLOR_BGR2BGRA)

            # with transparency?
            has_alpha_channel = True

            data = cvimage.tostring()
            colorspace = GdkPixbuf.Colorspace.RGB
            width = cvimage.shape[1]
            height = cvimage.shape[0]
            row_stride = cvimage.strides[0]
            destroy_fn = None
            destroy_fn_data = None

            # creates the pixbuf.
            pix = GdkPixbuf.Pixbuf.new_from_data(
                data,
                colorspace,
                has_alpha_channel,
                _GDK_PIXBUF_BIT_PER_SAMPLE,
                width,
                height,
                row_stride,
                destroy_fn,
                destroy_fn_data,)

            # shows pixbuf in GtkImage.
            self.camera_image.set_from_pixbuf(pix)
