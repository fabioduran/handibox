import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst, GLib

gi.require_version('Clutter', '1.0')
from gi.repository import Clutter

gi.require_version('Cheese', '3.0')
from gi.repository import Cheese

import time


def cheese_init():
    """ Initialize libcheese, by initializing GStreamer. """
    Gst.init(None)

cheese_init()

width=128
height=100

# texture
video_texture = Clutter.Actor.new()

# camera init - Fake Cam
camera = Cheese.Camera.new(
            video_texture=video_texture,
            name=None,
            x_resolution=640,
            y_resolution=480)
try:
    camera.setup()
except GLib.GError as error:
    logging.warning(error)

list_of_cameras = camera.get_camera_devices()
print(list_of_cameras)
for cam in list_of_cameras:
    print("###############")
    print(cam.get_name())
    print(cam.get_properties('name'))
    device = cam.get_properties('device')[0]
    device_prop = device.get_properties()
    print(device_prop.get_string("device.path"))

    # print(cam.get_src())
# time.sleep(1)
