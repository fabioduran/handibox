#!/usr/bin/env python3

from gi.repository import Gio, Gtk
from .keypath import A11Y_CONFIGURATION_KEY, A11Y_MOUSE_KEY
from .device import Devices


class HandiboxSettings(Gtk.Dialog):
    def __init__(self, parent):
        self.configuration_key = Gio.Settings.new(A11Y_MOUSE_KEY)
        self.config_key = Gio.Settings.new(A11Y_CONFIGURATION_KEY)
        Gtk.Dialog.__init__(self,
                            "Settings",
                            parent,
                            0)
        self.parent = parent

        # content are of dialog.
        box = self.get_content_area()

        self.builder = Gtk.Builder()
        self.builder.add_from_resource("/org/gnome/handibox/handibox.ui")
        box_pref = self.builder.get_object("box_pref")
        box.add(box_pref)

        # header bar
        self.hdr_pref = self.builder.get_object("hdr_pref")
        self.set_titlebar(self.hdr_pref)

        self.loading = True
        # radiobuttons (1 - 9 positions).
        posvalue = self.config_key.get_int("posvalue")
        for number in range(1, 10):
            position = "pos" + str(number)
            position_object = self.builder.get_object(position)
            position_object.connect("toggled",
                                    self.on_button_toggled,
                                    number)
            if posvalue == number:
                position_object.set_active(True)
        self.loading = False

        self.dwell_time_scale = self.builder.get_object("dwell_time_scale")
        # --dwell-time=[0.2-3.0] Time to keep the pointer motionless
        self.dwell_time = self.configuration_key.get_double("dwell-time")
        self.dwell_time_scale.set_value(self.dwell_time)
        # "value-changed" emitted by the dwell time scale with the callback

        # function dwell_time_scale_moved
        self.dwell_time_scale.connect("value-changed",
                                      self.dwell_time_scale_moved)

        self.threshold_scale = self.builder.get_object("threshold_scale")
        # -t, --threshold=INT Ignore small pointer movements.
        # Range 0 - 30 pixels.
        self.threshold = self.configuration_key.get_int("dwell-threshold")
        self.threshold_scale.set_value(self.threshold)
        # "value-changed" emitted by the threshold scale with the callback

        # function threshold_scale_moved
        self.threshold_scale.connect("value-changed",
                                     self.threshold_scale_moved)

        # Reset configuration
        self.reset_button = self.builder.get_object("reset_button")
        self.reset_button.connect("clicked",
                                  self.reset_configurations)

        self.close_pref_button = self.builder.get_object("close_pref_button")
        self.close_pref_button.connect("clicked",
                                       self.close_dialog)

        # ratio scale
        # ~ self.ratio_scale = self.builder.get_object("ratio_scale")
        # ~ self.ratiovalue = self.config_key.get_double("ratiovalue")
        # ~ self.ratio_scale.set_value(self.ratiovalue)
        # ~ self.ratio_scale.connect("value-changed",
                                 # ~ self.ratio_scale_moved)

        # Camera selection
        camera_list = self.get_all_cameras()
        camera_liststore = Gtk.ListStore(str, int, int, int)
        # Default camera with settings
        camera_liststore.append(["Default Camera", -1, 0, 0])

        for camera in camera_list:
            camera_liststore.append(camera)

        self.selectcamera = self.builder.get_object("selectcamera")
        self.selectcamera.set_model(camera_liststore)
        # default camera
        if self.config_key.get_int("cameraid") == -1:
            self.selectcamera.set_active(0)
        else:
            for position, value in enumerate(camera_liststore):
                camera_id = value[1]
                if self.config_key.get_int("cameraid") == camera_id:
                    self.selectcamera.set_active(position)

        # Cellrenderers to render the data
        renderer_pixbuf = Gtk.CellRendererPixbuf()
        renderer_text = Gtk.CellRendererText()
        self.selectcamera.pack_start(renderer_pixbuf, False)
        self.selectcamera.pack_start(renderer_text, False)
        self.selectcamera.add_attribute(renderer_text, "text", 0)
        self.selectcamera.add_attribute(renderer_pixbuf, "stock_id", 1)
        self.selectcamera.connect("changed", self.select_camera)

        self.show_all()

    def get_all_cameras(self):
        devices = Devices()
        camera_list = devices.get_camera_list()
        return camera_list

    def apply_configurations(self):
        if not self.loading:
            print("apply configurations")

            # remove image and reset size.
            self.parent.camera_image.clear()

            # apply position.
            self.parent.set_window_position(self.config_key.get_int("posvalue"))

            # apply size.
            self.parent.resize(self.config_key.get_int("widthvalue"),
                               self.config_key.get_int("heightvalue"))

            self.parent.set_default_logo()

    def close_dialog(self, button):
        self.destroy()

    # Any signal from the dwell time scale
    def dwell_time_scale_moved(self, event):
        dwell_time_value = self.dwell_time_scale.get_value()
        self.configuration_key.set_double("dwell-time",
                                          dwell_time_value)

    # Any signal from the threshold scale
    def threshold_scale_moved(self, event):
        threshold_value = self.threshold_scale.get_value()
        self.configuration_key.set_int("dwell-threshold",
                                       threshold_value)

    # ~ def ratio_scale_moved(self, event):
        # ~ self.ratio_value = self.ratio_scale.get_value()
        # ~ width_win, height_win = Devices().get_geometry()

        # ~ print(self.ratio_value)
        # ~ if self.ratio_value > 0:
            # ~ width_app = width_win * 0.234260615
            # ~ height_app = height_win * 0.3125
        # ~ else:
            # ~ width_app = width_win / 0.234260615
            # ~ height_app = height_win / 0.3125

        # ~ ratio_value_w = self.ratio_value * width_app
        # ~ ratio_value_h = self.ratio_value * height_app

        # ~ self.config_key.set_int("widthvalue", ratio_value_w)
        # ~ self.config_key.set_int("heightvalue", ratio_value_h)
        # ~ self.config_key.set_double("ratiovalue", self.ratio_value)
        # ~ self.apply_configurations()

    # Callback function connected to reset button
    def reset_configurations(self, button):

        self.configuration_key.reset("dwell-time")
        self.configuration_key.reset("dwell-threshold")
        self.dwell_time = self.configuration_key.get_double("dwell-time")
        self.threshold = self.configuration_key.get_int("dwell-threshold")

        self.dwell_time_scale.set_value(self.dwell_time)

        self.threshold_scale.set_value(self.threshold)
        # center position.
        self.config_key.set_int("posvalue", 5)
        position_object = self.builder.get_object("pos5")
        position_object.set_active(True)        

        # getting the windows size
        # width_win, height_win = Devices().get_geometry()

        # getting the standar app size
        # width_app = width_win* 0.234260615
        # height_app = height_win*0.3125
        # FIXME
        width_app = 426
        height_app = 240

        # setting the standar app size
        self.config_key.set_int("widthvalue", width_app)
        self.config_key.set_int("heightvalue", height_app)

        # reset the standar value of the ratio
        # ~ self.ratio_scale.set_value(1.0)

        # Reset camera to default
        self.config_key.set_int("cameraid", -1)
        self.apply_configurations()

    def select_camera(self, combo):
        it = self.selectcamera.get_active_iter()
        model = self.selectcamera.get_model()
        camera_id = model[it][1]
        # height = model[it][2]
        # width = model[it][3]
        self.config_key.set_int("cameraid", camera_id)
        # self.config_key.set_int("heightvalue", height)
        # self.config_key.set_int("widthvalue", width)

    def on_button_toggled(self, button, position):
        # radiobuttons (1 - 9 positions).
        for i in range(1, 10):
            if position == i:
                # save position.
                self.config_key.set_int("posvalue", i)
                break
        self.apply_configurations()
