#!/usr/bin/env python3
# main.py
#
# Copyright 2020 Fabio Duran Verdugo
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import sys
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gio
from gi.repository.GdkPixbuf import Pixbuf
from .handibox import Handibox
from .keypath import AUTHORS, ARTISTS, WEBSITE, LOGO


class Application(Gtk.Application):
    def __init__(self):
        super().__init__(application_id='org.gnome.Handibox',
                         flags=Gio.ApplicationFlags.FLAGS_NONE)

    def do_activate(self):
        self.win = self.props.active_window
        if not self.win:
            self.win = Handibox(application=self)
        self.win.present()

    def on_about(self, action, param):
        
        logo_pix = Pixbuf.new_from_resource(LOGO)
        about_dialog = Gtk.AboutDialog(transient_for=self.win,
                                       modal=True,
                                       program_name='Handibox',
                                       authors=AUTHORS,
                                       artists=ARTISTS,
                                       license_type=Gtk.License.GPL_3_0,
                                       website=WEBSITE,
                                       logo=logo_pix)
        about_dialog.present()

    def on_quit(self, action, param):
        self.quit()


def main(version):
    app = Application()
    return app.run(sys.argv)
