#!/usr/bin/env python3

import gi
gi.require_version('Gdk', '3.0')
from gi.repository import Gdk

gi.require_version('Gst', '1.0')
from gi.repository import Gst

gi.require_version('Clutter', '1.0')
from gi.repository import Clutter

gi.require_version('Cheese', '3.0')
from gi.repository import Cheese


class Devices():
    def __init__(self):
        self.window = Gdk.get_default_root_window()

        self.screen = self.window.get_screen()
        self.display = self.window.get_display()
        self.seat = self.display.get_default_seat()
        self.pointer = self.seat.get_pointer()

    def get_window(self):
        return self.window

    def get_seat(self):
        return self.seat

    def get_display(self):
        return self.display

    def get_screen(self):
        return self.screen

    def get_geometry(self):
        x, y = self.get_pointer_xy()
        monitor = self.display.get_monitor_at_point(x, y)
        geometry = monitor.get_geometry()
        return geometry.width, geometry.height

    def get_pointer(self):
        return self.pointer

    def get_pointer_xy(self):
        pointer_position = self.pointer.get_position_double()
        return pointer_position.x, pointer_position.y

    def get_camera_list(self):
        # FIXME: don't understand...
        Gst.init(None)

        # texture
        video_texture = Clutter.Actor.new()
        camera = Cheese.Camera.new(video_texture=video_texture,
                                   name=None,
                                   x_resolution=426,
                                   y_resolution=240)
        camera.setup()
        camera_values = []
        list_of_cameras = camera.get_camera_devices()

        # foreach Cheese.CameraDevice.
        for cam in list_of_cameras:
            camera_name = cam.get_name()
            camera_format = cam.get_best_format()
            height = camera_format.height
            width = camera_format.width

            device = cam.get_properties('device')[0]
            device_prop = device.get_properties()

            # provider: v4l2deviceprovider or pipewire-proplist.
            # print(device_prop.to_string())
            dev_path = ""
            if device_prop.get_name() == "v4l2deviceprovider":
                print("v4l2deviceprovider")
                dev_path = device_prop.get_string("device.path")
            elif device_prop.get_name() == "pipewire-proplist":
                print("pipewire-proplist")
                dev_path = device_prop.get_string("api.v4l2.path")

            camera_id = dev_path.split("/dev/video")[1]
            camera_values.append([camera_name, int(camera_id), height, width])

        return camera_values
