#!/usr/bin/env python3

import cairo
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gio, Gtk
from gi.repository.GdkPixbuf import Pixbuf
from .camera import Camera
from .handiboxsettings import HandiboxSettings
from .device import Devices
from .keypath import A11Y_APPLICATIONS_KEY, A11Y_MOUSE_KEY, LOGO
import threading


class Handibox(Gtk.ApplicationWindow):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.set_resizable(False)

        self.config_key_a11y_applications = Gio.Settings.new(A11Y_APPLICATIONS_KEY)
        self.config_key_a11y_mouse = Gio.Settings.new(A11Y_MOUSE_KEY)

        # 16:9 resolution.
        # keys: cameraid, heightvalue=240, widthvalue=426, posvalue, ratiovalue (not used)
        self.config_key = Gio.Settings.new("org.gnome.handibox")

        self.builder = Gtk.Builder()
        self.builder.add_from_resource("/org/gnome/handibox/handibox.ui")

        # Get main Box.
        self.box = self.builder.get_object("box_main")
        self.add(self.box)

        # Get header bar.
        self.hdr_bar = self.builder.get_object("hdr_bar")
        self.set_titlebar(self.hdr_bar)

        # First switch: Mousetweak. Enable/disable camera and mouse pointer.
        self.mousetweakswitch = self.builder.get_object("mousetweakswitch")
        self.mousetweakswitch.set_active(False)
        self.mousetweakswitch.connect("notify::active",
                                      self.on_click_enable_mousetweak)

        # Second switch: Screen Magnifier (zoom).
        self.magnifierswitch = self.builder.get_object("magnifierswitch")
        self.magnifierswitch.set_active(False)
        self.magnifierswitch.connect("notify::active",
                                     self.enable_a11y_application,
                                     "screen-magnifier-enabled")

        # Third switch: Screen Keyboard.
        self.keyboardswitch = self.builder.get_object("keyboardswitch")
        self.keyboardswitch.set_active(False)
        self.keyboardswitch.connect("notify::active",
                                    self.enable_a11y_application,
                                    "screen-keyboard-enabled")

        # Button close app.
        self.btn_close = self.builder.get_object("btn_close")
        self.btn_close.connect("clicked",
                               # kwargs["application"].on_quit, None)
                               self.app_quit,
                               kwargs["application"]
                               )

        # Button view settings.
        self.btn_pref = self.builder.get_object("btn_pref")
        self.btn_pref.connect("clicked",
                              self.on_click_view_settings)

        # Button About
        self.btn_about = self.builder.get_object("btn_about")
        self.btn_about.connect("clicked",
                               kwargs["application"].on_about, None)

        # Get widget (GtkImage).
        self.camera_image = Gtk.Image()
        self.set_default_logo()

        self.box.add(self.camera_image)

        # Set window alway on top.
        self.set_keep_above(True)

        # Get and apply saved preferences.
        # self.old_ratio = self.config_key.get_double("ratiovalue") (not used)

        # Resize window/image.
        self.resize(self.config_key.get_int("widthvalue"),
                    self.config_key.get_int("heightvalue"))

        # for transparency.
        self.connect('draw', self.draw)
        screen = self.get_screen()
        visual = screen.get_rgba_visual()
        if visual and screen.is_composited():
            self.set_visual(visual)
        self.set_app_paintable(True)

        # Set window position.
        self.set_window_position(self.config_key.get_int("posvalue"))

        self.show_all()

    def draw(self, widget, context):
        context.set_source_rgba(0, 0, 0, 0)
        context.set_operator(cairo.OPERATOR_SOURCE)
        context.paint()
        context.set_operator(cairo.OPERATOR_OVER)

    def set_default_logo(self):
        logo_pix = Pixbuf.new_from_resource(LOGO)
        self.camera_image.set_from_pixbuf(logo_pix)

    def enable_disable_camera(self, state):
        if state==True:
            self.camera = Camera(self.camera_image)
            # create a thread to capture and process images.
            self.thread = threading.Thread(target=self.camera._init_thread)
            # run thread as a daemon.
            self.thread.daemon = True
            # start thread.
            self.thread.start()
        else:
            # replace for default image (logo).
            self.set_default_logo()
            # for controlling the loop that captures images. leave the loop.
            self.camera.run = False
            # stop thread.
            self.thread.join()
            # release camera.
            self.camera.capture.release()

    def on_click_enable_mousetweak(self, button, active):
        if button.get_active():
            # enable mouse.
            self.config_key_a11y_mouse.set_boolean("dwell-click-enabled",
                                                   True)
            self.enable_disable_camera(state=True)
        else:
            # disable mouse.
            self.config_key_a11y_mouse.set_boolean("dwell-click-enabled",
                                                   False)
            self.enable_disable_camera(state=False)

    def enable_a11y_application(self, button, active, app_key):
        if button.get_active():
            self.config_key_a11y_applications.set_boolean(app_key,
                                                          True)
        else:
            self.config_key_a11y_applications.set_boolean(app_key,
                                                          False)

    def disable_a11y_application(self):
        self.config_key_a11y_mouse.set_boolean("dwell-click-enabled",
                                               False)
        self.config_key_a11y_applications.set_boolean("screen-keyboard-enabled",
                                                      False)
        self.config_key_a11y_applications.set_boolean("screen-magnifier-enabled",
                                                      False)

    def on_click_view_settings(self, widget):
        settings = HandiboxSettings(self)

        # waiting until window dialog is closed.
        response = settings.run()
        if response == Gtk.ResponseType.CANCEL:
            settings.destroy()

    def set_window_position(self, position):
        # get sizes from Gtk.ApplicationWindow.
        #width, height = self.get_size()
        width = self.config_key.get_int("widthvalue")
        height = self.config_key.get_int("heightvalue")
        # get sizes from screen.
        width_screen, height_screen = Devices().get_geometry()

        geometry_operation = {1: (0, 0),
                              2: (width_screen/2-width/2, 0),
                              3: (width_screen-width, 0),
                              4: (0, height_screen/2-height/2),
                              5: (width_screen/2-width/2, height_screen/2-height/2),
                              6: (width_screen-width, height_screen/2-height/2),
                              7: (0, height_screen-height),
                              8: (width_screen/2-width/2, height_screen-height),
                              9: (width_screen-width, height_screen-height)}

        width, height = geometry_operation.get(position)
        self.move(width, height)

    def app_quit(self, event, app):
        self.disable_a11y_application()
        app.on_quit(None, None)
