# Dependences

    Python3 > 3.5
    PyGobject > 3.36
    python3-opencv (4.2.0 last testing)
    libcheese > 3.36
	meson > 0.50

## Install on Fedora
```
dnf install python3-opencv
dnf install cheese-libs-devel
dnf install meson
```

## Install on Debian or Ubuntu
```
apt install python3-opencv
apt install libcheese libcheese-dev
apt install meson
```

# Install

```
$ git clone https://gitlab.gnome.org/fabioduran/handibox.git
$ cd handibox
$ mkdir build
$ cd build
$ meson ../ --prefix $PWD
$ ninja
$ ninja install
$ export GSETTINGS_SCHEMA_DIR=$PWD/share/glib-2.0/schemas/
$ ./bin/handibox
```


# Testing On

    * Ubuntu 20.04
    * Ubuntu 20.10
    * Debian Testing
	* Fedora 33 - 34

# Prompt

* For a better experience activate animations in tweakstools.  
	- https://wiki.gnome.org/Apps/Tweaks
	- https://gitlab.gnome.org/GNOME/gnome-tweaks
# Video Examples

* https://www.youtube.com/watch?v=GJGWoNGmgN4
* https://www.youtube.com/watch?v=kzhIeqKqqQ4


